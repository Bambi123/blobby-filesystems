// Copyright (c) Thomas French

//COMP1521 Blobby Assignment 2
//Thomas French
//error.h helper library header file

#include <stdio.h>
#include <stdint.h>

/* error_file_open()
This error function is called whenever a blob pathname cannot be opened into a file stream in order
for either a read or a write operation in the sub code of "blobby.c".  This function calls
perror() and then shortly executes the entire program with an exit code of 1.
*/
void error_file_open(char *filename);

/* error_magic_number()
This error function is called whenever a BLOBETTE_MAGIC_NUMBER is expected by the sub code of 
"blobby.c" and one is not returned.  
*/
void error_magic_number();

/*
This error function is called whenever a blobette pathname cannot be opened into a file stream in order
for either a read or write operation in the sub code of "blobby.c".  This function prints to stderr
and then promptly exits the program.
*/
void error_read_filename();

/*
This error function is called whenever a string is unable to have a section of memory allocated
to it.  The function prints to stderr and then promptly exits the program.  
*/
void error_memory();

/* error_permission()
This error function is called whenever a file mode indicated a false permission in the file.  
This function calls perror() and then shortly executes the entire program with an exit code of 1.
*/
void error_permission(char *filename);

/* error_hash()
The error function is called whenever the hash value doesn't correctly match the given hash value
stored in the blobette inside the blob.  This function prints to stderr and the promptly exits the 
program.  
*/
void error_hash();

/* error_stat_struct()
This error function is called whenever a filename fails to return a struct stat with a valid return.
This function calls perror() and then shortly executes the entire program with an exit code of 1.
*/
void error_stat_struct(char *filename);
