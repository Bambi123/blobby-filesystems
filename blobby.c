// Copyright (c) Thomas French

// blobby.c
// blob file archiver
// COMP1521 20T3 Assignment 2
// Written by <Thomas French>

//Thomas French z5206283
//The following code is written in completion of Computer System Fundamentals, UNSW, COMP1521
//Assignment 2 - "blobby.c" - Is a file directory system that can compute a number of arguments
//to list, extract, and add files.
//The structure of files in this assignment are known as blobs.  With accordance of the 
//COMP1521 assignment 2 spec, these .blob files contain smaller files with respective file
//details known as blobettes.  

//Details about all author designed functions can be found in "ass2.h", a library header file
//submitted along side "blobby.c".

//Details about all eauthor designed error functions can be found in "error.h", a library header
//file submitted along side "blobby.c".

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "error.h"
#include "ass2.h"

// the first byte of every blobette has this value
#define BLOBETTE_MAGIC_NUMBER          0x42

// number of bytes in fixed-length blobette fields
#define BLOBETTE_MAGIC_NUMBER_BYTES    1
#define BLOBETTE_MODE_LENGTH_BYTES     3
#define BLOBETTE_PATHNAME_LENGTH_BYTES 2
#define BLOBETTE_CONTENT_LENGTH_BYTES  6
#define BLOBETTE_HASH_BYTES            1

// maximum number of bytes in variable-length blobette fields
#define BLOBETTE_MAX_PATHNAME_LENGTH   65535
#define BLOBETTE_MAX_CONTENT_LENGTH    281474976710655

//ADD YOUR DEFINITIONS HERE
#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0

#define MODE_BYTES      3
#define PATH_BYTES      2
#define CONTENT_BYTES   6
#define HASH_BYTES      1


typedef enum action {
    a_invalid,
    a_list,
    a_extract,
    a_create
} action_t;


void usage(char *myname);
action_t process_arguments(int argc, char *argv[], char **blob_pathname,
                           char ***pathnames, int *compress_blob);

void list_blob(char *blob_pathname);
void extract_blob(char *blob_pathname);
void create_blob(char *blob_pathname, char *pathnames[], int compress_blob);

uint8_t blobby_hash(uint8_t hash, uint8_t byte);

//###################################################################
//All function prototypes can be found in ass2.h
//###################################################################


// YOU SHOULD NOT NEED TO CHANGE main, usage or process_arguments

int main(int argc, char *argv[]) {
    char *blob_pathname = NULL;
    char **pathnames = NULL;
    int compress_blob = 0;
    action_t action = process_arguments(argc, argv, &blob_pathname, &pathnames,
                                        &compress_blob);

    switch (action) {
    case a_list:
        list_blob(blob_pathname);
        break;

    case a_extract:
        extract_blob(blob_pathname);
        break;

    case a_create:
        create_blob(blob_pathname, pathnames, compress_blob);
        break;

    default:
        usage(argv[0]);
    }

    return 0;
}

// print a usage message and exit

void usage(char *myname) {
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "\t%s -l <blob-file>\n", myname);
    fprintf(stderr, "\t%s -x <blob-file>\n", myname);
    fprintf(stderr, "\t%s [-z] -c <blob-file> pathnames [...]\n", myname);
    exit(1);
}

// process command-line arguments
// check we have a valid set of arguments
// and return appropriate action
// **blob_pathname set to pathname for blobfile
// ***pathname set to a list of pathnames for the create action
// *compress_blob set to an integer for create action

action_t process_arguments(int argc, char *argv[], char **blob_pathname,
                           char ***pathnames, int *compress_blob) {
    extern char *optarg;
    extern int optind, optopt;
    int create_blob_flag = 0;
    int extract_blob_flag = 0;
    int list_blob_flag = 0;
    int opt;
    while ((opt = getopt(argc, argv, ":l:c:x:z")) != -1) {
        switch (opt) {
        case 'c':
            create_blob_flag++;
            *blob_pathname = optarg;
            break;

        case 'x':
            extract_blob_flag++;
            *blob_pathname = optarg;
            break;

        case 'l':
            list_blob_flag++;
            *blob_pathname = optarg;
            break;

        case 'z':
            (*compress_blob)++;
            break;

        default:
            return a_invalid;
        }
    }

    if (create_blob_flag + extract_blob_flag + list_blob_flag != 1) {
        return a_invalid;
    }

    if (list_blob_flag && argv[optind] == NULL) {
        return a_list;
    } else if (extract_blob_flag && argv[optind] == NULL) {
        return a_extract;
    } else if (create_blob_flag && argv[optind] != NULL) {
        *pathnames = &argv[optind];
        return a_create;
    }

    return a_invalid;
}


void list_blob(char *blob_pathname) {

    FILE * stream = fopen(blob_pathname, "r");

    if (!stream){
        error_file_open(blob_pathname);
    }


    int c = fgetc(stream);
    if (c != BLOBETTE_MAGIC_NUMBER){error_magic_number();}



    while (c != EOF){

        if ( !(c == BLOBETTE_MAGIC_NUMBER) ){
            error_magic_number();
        } 

        struct blobette_bytes b = read_blobette_details(stream);
        fprintf(stdout, "%06lo %5lu %s\n", b.mode, b.content_length, b.pathname);

        read_free(b);

        c = fgetc(stream);

    }

    fclose(stream);

}

void extract_blob(char *blob_pathname) {

    FILE * stream = fopen(blob_pathname, "r");
    if (!stream){
        error_file_open(blob_pathname);
    }

    int c = fgetc(stream);
    if (c != BLOBETTE_MAGIC_NUMBER){error_magic_number();}


    while (c != EOF){

        if (c != BLOBETTE_MAGIC_NUMBER){
            error_magic_number();
        }

        struct blobette_bytes b = read_blobette_details(stream);
        if (!b.valid){error_hash();}
        printf("Extracting: %s\n", b.pathname);

        create_new_file(b);
        read_free(b);

        c = fgetc(stream);
    }

    fclose(stream);

}

// create blob_pathname from NULL-terminated array pathnames
// compress with xz if compress_blob non-zero (subset 4)

void create_blob(char *blob_pathname, char *pathnames[], int compress_blob) {

    FILE * stream = fopen(blob_pathname, "w");
    if (!stream){error_file_open(blob_pathname);}

    for (int p = 0; pathnames[p]; p++) {

        char *filename = pathnames[p];
        printf("Adding: %s\n", filename);
        struct blobette_bytes b = get_blobette(filename);
        put_blobette_details(b, stream);
        
        read_free(b);

    }

}

//###################################################################
//All written functions for this assignment prototypes in ass2.h can be found below
//###################################################################

void next_get(uint8_t *hash, int *c, FILE * stream){

    *c = fgetc(stream);
    *hash = blobby_hash(*hash, *c);
    if ( !(*hash >= 0 && *hash <= 255) ){error_hash();}

}

void next_put(uint8_t *hash, int *c, FILE * stream){

    fputc(*c, stream);
    *hash = blobby_hash(*hash, *c);
    if ( !(*hash >= 0 && *hash <= 255) ){error_hash();}

}

struct blobette_bytes read_blobette_details(FILE * stream){

    //3 bytes for the mode
    //2 bytes for the pathname length
    //2 bytes for the content length

    uint8_t hash = blobby_hash(0, BLOBETTE_MAGIC_NUMBER);

    struct blobette_bytes b;

    b.mode = read_bytes_int(stream, MODE_BYTES, &hash);
    b.pathname_length = read_bytes_int(stream, PATH_BYTES, &hash);
    b.content_length = read_bytes_int(stream, CONTENT_BYTES, &hash);
    
    b.pathname = read_bytes_string(stream, b.pathname_length, &hash);
    b.content = read_bytes_string(stream, b.content_length, &hash);

    b.hash = fgetc(stream); //no need to calculate the hash value of this number

    if (b.hash != hash){
        b.valid = 0; // invalid
    } else {
        b.valid = 1; // valid
    }

    return b;
}

uint64_t read_bytes_int(FILE * stream, int n, uint8_t *hash){

    uint64_t bytes = 0;
    int c = 0;
    for (int i = 0; i < n; i++){

        next_get(hash, &c, stream);
        bytes = bytes << 8;
        bytes = (bytes | c);

    }

    return bytes;
}

char * read_bytes_string(FILE * stream, uint64_t n, uint8_t *hash){

    char *string = malloc((n + 1) * sizeof(char) );
    if (!string){
        error_memory();
    }
    int c = 0;
    for (int i = 0; i < n; i++){

        next_get(hash, &c, stream);

        string[i] = c;

    }
    string[n] = '\0';

    return string;
}

void read_free(struct blobette_bytes b){
    free(b.pathname);
    free(b.content);
}

void create_new_file(struct blobette_bytes b){

    FILE * stream = fopen(b.pathname, "w");
    if (!stream){
        error_file_open(b.pathname);
    }

    for (int i = 0; i < b.content_length; i++){
        fputc(b.content[i], stream);
    }

    if (chmod(b.pathname, b.mode) != 0){
        error_permission(b.pathname);
    }

    fclose(stream);
}

struct blobette_bytes get_blobette(char *filename){

    FILE * stream = fopen(filename, "r");
    if (!stream){error_read_filename();}

    struct stat s;
    if (stat(filename, &s) != 0){error_stat_struct(filename);}

    struct blobette_bytes b;

    b.mode = s.st_mode;

    b.pathname_length = strlen(filename);
    b.content_length = s.st_size;


    b.pathname = malloc((b.pathname_length + 1) * sizeof(char));
    if (!b.pathname){error_memory();}

    for (int i = 0; i < b.pathname_length; i++){
        b.pathname[i] = filename[i];
    }
    b.pathname[b.pathname_length] = '\0';

    b.content = malloc((b.content_length + 1) * sizeof(char));
    if (!b.content){error_memory();}
    for (int i = 0; i < b.content_length; i++){
        b.content[i] = fgetc(stream);
    }
    b.content[b.content_length] = '\0';

    b.hash = 0; // this gets calculated later on, initially zero
    b.valid = 1; // initially valid

    fclose(stream);
    return b;
}

void put_blobette_details(struct blobette_bytes b, FILE * stream){

    put_magic_number(stream, &b.hash);

    put_bytes_int(b.mode, MODE_BYTES, stream, &b.hash);
    put_bytes_int(b.pathname_length, PATH_BYTES, stream, &b.hash);
    put_bytes_int(b.content_length, CONTENT_BYTES, stream, &b.hash);

    put_bytes_string(b.pathname, b.pathname_length, stream, &b.hash);
    put_bytes_string(b.content, b.content_length, stream, &b.hash);

    fputc(b.hash, stream); // no need to call next_put(), hash is already calculated

}

void put_magic_number(FILE * stream, uint8_t *hash){

    int c = BLOBETTE_MAGIC_NUMBER;
    next_put(hash, &c, stream);

}

void put_bytes_int(uint64_t var, int n, FILE * stream, uint8_t *hash){

    uint64_t comp = 0b0000000000000000000000000000000000000000000000000000000011111111;

    for (int i = 1; i < n; i++){
        comp = comp << 8; 
    } // sets the comparison to be in the correct position to extract the first byte

    for (int i = 0; i < n; i++){

        int c = (var & comp) >> ((n - 1 - i) * 8); //algorithm for bit shift
        comp = comp >> 8;

        next_put(hash, &c, stream);

    }

}

void put_bytes_string(char *string, int n, FILE * stream, uint8_t *hash){

    for (int i = 0; i < n; i++){
        int c = string[i];
        next_put(hash, &c, stream);
    }

}

void print_to_binary(uint64_t n){

    uint64_t comp = 0b1000000000000000000000000000000000000000000000000000000000000000;

    while (comp){
        if ( (n & comp) != 0){
            printf("1");
        } else {
            printf("0");
        }
        comp = comp >> 1;
    }
    printf("\n");

}

//###################################################################
//End of all written functions ptototypes in ass2.h
//###################################################################


// YOU SHOULD NOT CHANGE CODE BELOW HERE

// Lookup table for a simple Pearson hash
// https://en.wikipedia.org/wiki/Pearson_hashing
// This table contains an arbitrary permutation of integers 0..255

const uint8_t blobby_hash_table[256] = {
    241, 18,  181, 164, 92,  237, 100, 216, 183, 107, 2,   12,  43,  246, 90,
    143, 251, 49,  228, 134, 215, 20,  193, 172, 140, 227, 148, 118, 57,  72,
    119, 174, 78,  14,  97,  3,   208, 252, 11,  195, 31,  28,  121, 206, 149,
    23,  83,  154, 223, 109, 89,  10,  178, 243, 42,  194, 221, 131, 212, 94,
    205, 240, 161, 7,   62,  214, 222, 219, 1,   84,  95,  58,  103, 60,  33,
    111, 188, 218, 186, 166, 146, 189, 201, 155, 68,  145, 44,  163, 69,  196,
    115, 231, 61,  157, 165, 213, 139, 112, 173, 191, 142, 88,  106, 250, 8,
    127, 26,  126, 0,   96,  52,  182, 113, 38,  242, 48,  204, 160, 15,  54,
    158, 192, 81,  125, 245, 239, 101, 17,  136, 110, 24,  53,  132, 117, 102,
    153, 226, 4,   203, 199, 16,  249, 211, 167, 55,  255, 254, 116, 122, 13,
    236, 93,  144, 86,  59,  76,  150, 162, 207, 77,  176, 32,  124, 171, 29,
    45,  30,  67,  184, 51,  22,  105, 170, 253, 180, 187, 130, 156, 98,  159,
    220, 40,  133, 135, 114, 147, 75,  73,  210, 21,  129, 39,  138, 91,  41,
    235, 47,  185, 9,   82,  64,  87,  244, 50,  74,  233, 175, 247, 120, 6,
    169, 85,  66,  104, 80,  71,  230, 152, 225, 34,  248, 198, 63,  168, 179,
    141, 137, 5,   19,  79,  232, 128, 202, 46,  70,  37,  209, 217, 123, 27,
    177, 25,  56,  65,  229, 36,  197, 234, 108, 35,  151, 238, 200, 224, 99,
    190
};

// Given the current hash value and a byte
// blobby_hash returns the new hash value
//
// Call repeatedly to hash a sequence of bytes, e.g.:
// uint8_t hash = 0;
// hash = blobby_hash(hash, byte0);
// hash = blobby_hash(hash, byte1);
// hash = blobby_hash(hash, byte2);
// ...

uint8_t blobby_hash(uint8_t hash, uint8_t byte) {
    return blobby_hash_table[hash ^ byte];
}
