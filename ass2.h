// Copyright (c) Thomas French

//Thomas French z5206283
//COMP1521 Assignment 2
//ass2.h helper library header file

#include <stdio.h>

/* struct blobette_bytes
This structure addresses the method in which we structure the byte patterns retrieved from
each individual blobette inside blob files.  Each bit section is run through with bit operators
and is calculated via the implementation of sub functions found in "blobby.c".  This structure
is passed back and forth from the main list_blob(), extract_blob, and create_blob() functions.  
*/

struct blobette_bytes{
    uint64_t mode;
    uint64_t pathname_length;
    uint64_t content_length;
    char *pathname;
    char *content;
    uint8_t hash;
    int valid;
};

/* next_get()
This function both retrieves the next character from a file stream used fgetc(), as well 
as updates the hash value.  Both the character and hash are given as pointer addresses so
both values can be stored in respective higher functions.  
*/

void next_get(uint8_t *hash, int *c, FILE * stream);

/* next_put()
This function both prints the next character from a file stream used fputc(), as well 
as updates the hash value.  Both the character and hash are given as pointer addresses so
both values can be stored in respective higher functions.  
*/

void next_put(uint8_t *hash, int *c, FILE * stream);

/* read_blobette_details()
This function calls next_get(), read_free(), read_bytes_int() and read_bytes_string() in order
to read in the required bytes from each blobette.  Error checking of the BLOBETTE_MAGIC_NUMBER
is done before this function is called.  The bytes are read in from the respective file stream
and the return is a structure of blobette_bytes.  
*/

struct blobette_bytes read_blobette_details(FILE * stream);

/* read_free()
This function frees all the allocated memory in and of the blobette_byte structures.
These mallocc'ed memory include the string sections of the structure, i.e. content and pathname
*/

void read_free(struct blobette_bytes);

/* read_bytes_int()
This function collects a total of 'n' bytes from the file stream using the next_get() function.
The function can be used to read in three sections of the blobette_bytes structure.  The mode,
the pathname length, and the content length.  This function returns the respective number of
bytes as an unsigned long integer of 64 bits.  
*/

uint64_t read_bytes_int(FILE * stream, int n, uint8_t *hash);

/*
This function collects a total of 'n' bytes from the file stream using the next_get() function.
The function can be used to read in two sections of the blobette_bytes structure.  The pathname
and the content of the blobette.  This function returns the respective number of bytes as a 
string with allocated memory.  
*/

char *read_bytes_string(FILE * stream, uint64_t n, uint8_t *hash);

/* create_new_file
This function creates a new file based upon the content of the blobette_bytes structure.
The function opens the file stream from the pathname as a write file and rewrites the content
into the file.  The function also changes the permission of the new file to the permission
specified in the mode section of the blobette_bytes structure.  
*/

void create_new_file(struct blobette_bytes b);

/* get_blobette()
This function opens the file stream from the given filename and generates all the required
blobette information based on the specifications of the file.  The details of the blobette
are returned from the function in fht eofmr of a blobette_bytes structure.  
*/

struct blobette_bytes get_blobette(char *filename);

/* put_blobette_details()
This function puts all of the bytes from the blobette_byte structure into the given file stream.
The function calls put_magic_number(), put_bytes_int() and put_bytes_string in order to do this.
*/

void put_blobette_details(struct blobette_bytes b, FILE * stream);

/* put_magic_number()
This function calls next_put() to insert the BLOBETTE_MAGIC_NUMBER into the opened file in
the file stream.  
*/

void put_magic_number(FILE * stream, uint8_t *hash);

/* put_bytes_int()
This function calls next_put() to insert a series of 'n' bytes into the file stream.  It
deconstructs the given variable 'var' into singular bytes and inserts them into the
stream.
*/

void put_bytes_int(uint64_t var, int n, FILE * stream, uint8_t *hash);

/* put_bytes_string()
This function calls next_put() to insert a series of 'n' bytes into the file stream.  It
deconstructs the given variable 'string' into singular bytes and inserts them into the
stream.
*/

void put_bytes_string(char *string, int n, FILE * stream, uint8_t *hash);

/* print_to_binary()
This function is simply used as a debugging function and is not used in the code of "blobby.c".
The function takes any long integer varible and prints the corresponding binary format of
the integer.
*/

void print_to_binary(uint64_t n);

/* next_put()
This function both prints the next character from a file stream used fputc(), as well 
as updates the hash value.  Both the character and hash are given as pointer addresses so
both values can be stored in respective higher functions.  
*/

void next_put(uint8_t *hash, int *c, FILE * stream);
