// Copyright (c) Thomas French

//COMP1521 Blobby Assignment 2
//Thomas French
//error.c helper library

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0

void error_file_open(char *filename){
    perror(filename);
    exit(EXIT_FAILURE);
}

void error_magic_number(){
    fprintf(stderr, "ERROR: Magic byte of blobette incorrect\n");
    exit(EXIT_FAILURE);
}

void error_read_filename(){
    fprintf(stderr, "ERROR: Filename read incorrectly!\n");
    exit(EXIT_FAILURE);
}

void error_memory(){
    fprintf(stderr, "ERROR: Memory was not able to be allocated!\n");
    exit(EXIT_FAILURE);
}

void error_permission(char *filename){
    perror(filename);
    exit(EXIT_FAILURE);
}

void error_hash(){
    fprintf(stderr, "ERROR: blob hash incorrect\n");
    exit(EXIT_FAILURE);
}

void error_stat_struct(char *filename){
    perror(filename);
    exit(EXIT_FAILURE);
}
